import { createBrowserHistory } from "history";
import React, { createRef, useState } from "react";
import { Route, Router, Switch } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import {
  Container,
  Dimmer,
  Grid,
  Loader,
  Statistic,
  Sticky,
} from "semantic-ui-react";
import AccountSelector from "./AccountSelector";
import BlockNumber from "./BlockNumber";
import Events from "./Events";
import SensioModule from "./sensio";
import CreateProof from "./sensio/poe/proofs/CreateProof";
import ProofsInfo from "./sensio/poe/proofs/ProofsInfo";
import ProofsList from "./sensio/poe/proofs/ProofsList";
import CreateRule from "./sensio/poe/rules/CreateRule";
import RulesInfo from "./sensio/poe/rules/RulesInfo";
import RulesList from "./sensio/poe/rules/RulesList";
import { SubstrateContextProvider, useSubstrate } from "./substrate-lib";
import { DeveloperConsole } from "./substrate-lib/components";
const history = createBrowserHistory();

function Main() {
  const [accountAddress, setAccountAddress] = useState(null);
  const { apiState, keyring, keyringState } = useSubstrate();
  const accountPair =
    accountAddress &&
    keyringState === "READY" &&
    keyring.getPair(accountAddress);

  const loader = (text) => (
    <Dimmer active>
      <Loader size="small">{text}</Loader>
    </Dimmer>
  );

  if (apiState === "ERROR") return loader("Error connecting to the blockchain");
  else if (apiState !== "READY") return loader("Connecting to the blockchain");

  if (keyringState !== "READY") {
    return loader(
      "Loading accounts (please review any extension's authorization)"
    );
  }

  const contextRef = createRef();

  return (
    <Router history={history}>
      <div ref={contextRef}>
        <Sticky context={contextRef}>
          <AccountSelector setAccountAddress={setAccountAddress} />
        </Sticky>

        <Container>
          <Grid stackable columns="equal">
            <Grid.Row stretched>
              <Statistic.Group size="mini">
                <RulesInfo />
                <ProofsInfo />
                <BlockNumber />
                <BlockNumber finalized />
              </Statistic.Group>
            </Grid.Row>
            <Grid.Row>
              <Switch>
                <Route path="/poe/create-rule">
                  <CreateRule accountPair={accountPair} />
                </Route>
                <Route path="/poe/create-proof">
                  <CreateProof accountPair={accountPair} />
                </Route>
                <Route path="/poe/rules">
                  <RulesList accountPair={accountPair} />
                </Route>
                <Route path="/poe/proofs">
                  <ProofsList accountPair={accountPair} />
                </Route>

                <Route path="/">
                  <SensioModule accountPair={accountPair} />
                </Route>
              </Switch>
            </Grid.Row>
            <Grid.Row style={{ bottom: 0, marginTop: 64 }}>
              <Events />
            </Grid.Row>
            <DeveloperConsole />
          </Grid>
        </Container>
      </div>
    </Router>
  );
}

export default function App() {
  return (
    <SubstrateContextProvider>
      <Main />
    </SubstrateContextProvider>
  );
}
