import React from "react";
import { Button, Modal } from "semantic-ui-react";

const CodeModal = ({ data, title, header }) => (
  <Modal
    trigger={<Button secondary>{title ? title : "Show"}</Button>}
    closeIcon
  >
    <Modal.Header>{header || "Rule Payload"}</Modal.Header>
    <Modal.Content scrolling>
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </Modal.Content>
  </Modal>
);

export default CodeModal;
