import { hexToString } from "@polkadot/util";
import React, { useEffect, useState } from "react";
import {
  Button,
  Container,
  Divider,
  Grid,
  Icon,
  Input,
  List,
} from "semantic-ui-react";
import { useSubstrate } from "../../../substrate-lib";
import CodeModal from "../CodeModal";
import { getProofs } from "../helpers";

export default function ProofsList({ accountPair }) {
  const { api } = useSubstrate();
  const [allProofs, seAllProofs] = useState([]);
  const [proofs, setProofs] = useState([]);
  const [interval, setCurrentInterval] = useState(0);

  useEffect(() => {
    const getInfo = async () => {
      try {
        const p = await getProofs(api);
        const _p = p.map(({ proof, accountId, blockNumber }) => {
          const { proofId, body, createdAt, ruleId, forWhat } = proof;

          return {
            proof: {
              ...proof,
              body: {
                ...JSON.parse(hexToString(body.toString())),
                ruleId: hexToString(ruleId.toString()),
              },
              proofId: hexToString(proofId.toString()),
              ruleId: hexToString(ruleId.toString()),
              forWhat: forWhat.toString(),
              createdAt: new Date(createdAt.toNumber()),
            },
            accountId: accountId.toString(),
            blockNumber,
          };
        });
        seAllProofs(_p);
        setProofs(_p);
      } catch (e) {
        console.error(e);
      }
    };
    if (!interval) {
      getInfo();
    } else {
      setCurrentInterval(setInterval(getInfo, 3000));
    }
    return () => {
      clearInterval(interval);
    };
  }, [api, interval]);

  function handleSearch(e, { name, value }) {
    if (!value) {
      setProofs(allProofs);
      return;
    }
    const ps = proofs.filter((p) => p.proof.proofId === value);
    setProofs(ps);
  }

  return (
    <Container>
      <Grid>
        <Grid.Row color={"purple"}>
          <Grid.Column width={4}>
            <h1 style={{ marginLeft: 16 }}>PoE Rules</h1>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width="13">
            <Input
              fluid
              focus
              icon="search"
              placeholder="Proof ID ..."
              onChange={handleSearch}
            />
          </Grid.Column>
          <Grid.Column width="3">
            <Button onClick={() => setProofs(allProofs)}>
              Show All proofs
            </Button>
          </Grid.Column>
        </Grid.Row>
        {proofs.length !== 0 && (
          <Grid.Row>
            <Grid.Column>
              <List divided verticalAlign="middle" size="large">
                {proofs.map(({ proof, accountId, blockNumber }, i) => {
                  const { proofId, body, createdAt, forWhat } = proof;

                  return (
                    <List.Item key={proofId}>
                      <List.Content floated="right">
                        <CodeModal
                          title="RAW"
                          header={"Proof payload"}
                          data={proof}
                        />
                      </List.Content>
                      <Icon
                        style={{
                          color:
                            accountId === accountPair.address ? "green" : "red",
                          fontSize: "2em",
                          paddingTop: 10,
                        }}
                        name={
                          accountId === accountPair.address ? "check" : "cancel"
                        }
                      />

                      <List.Content>
                        <List.Header style={{ paddingTop: 10 }}>
                          {forWhat} {proofId}
                        </List.Header>
                        <List.Description>
                          <div>
                            Owned by: <code>{body.owner}</code>
                          </div>
                          <div>
                            Created at:{" "}
                            <strong>{createdAt.toISOString()}</strong>
                          </div>
                          <Divider />
                          Assigned to the account <code>{accountId}</code>
                        </List.Description>
                        <List.Description>
                          <Divider />
                          <pre style={{ maxWidth: 1100 }}>
                            {JSON.stringify(body.params, null, 2)}
                          </pre>
                        </List.Description>
                      </List.Content>
                    </List.Item>
                  );
                })}
              </List>
            </Grid.Column>
          </Grid.Row>
        )}
      </Grid>
    </Container>
  );
}
