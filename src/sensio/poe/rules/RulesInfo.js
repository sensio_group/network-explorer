import React, { useEffect, useState } from "react";
import { Statistic } from "semantic-ui-react";
import { useSubstrate } from "../../../substrate-lib";
import { getRules } from "../helpers";

export default function RulesInfo(props) {
  const { api } = useSubstrate();
  const [rules, setRules] = useState([]);

  useEffect(() => {
    async function getInfo() {
      try {
        const r = await getRules(api);
        setRules(r);
      } catch (e) {
        console.error(e);
      }
    }
    getInfo();
    const i = setInterval(getInfo, 3000);
    return () => {
      clearInterval(i);
    };
  }, [api]);

  return <Statistic label={"PoE Rules"} value={rules.length} />;
}
