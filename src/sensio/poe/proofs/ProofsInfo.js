import React, { useEffect, useState } from "react";
import { Statistic } from "semantic-ui-react";
import { useSubstrate } from "../../../substrate-lib";
import { getProofs } from "../helpers";

export default function ProofsInfo(props) {
  const { api } = useSubstrate();
  const [proofs, setProofs] = useState([]);

  useEffect(() => {
    async function getInfo() {
      try {
        const r = await getProofs(api);
        setProofs(r);
      } catch (e) {
        console.error(e);
      }
    }
    getInfo();

    const i = setInterval(getInfo, 3000);
    return () => {
      clearInterval(i);
    };
  }, [api]);

  return <Statistic label={"PoE Proofs"} value={proofs.length} />;
}
