import { hexToString, stringToHex } from "@polkadot/util";
import CID from "cids";
import * as imghash from "imghash";
import mh from "multihashing-async";
import { cameraRule, lensRule, photoRule } from "./rules/defaultRules";
/**
 * @return {Promise<[{
      rule: Object,
      ruleId: string,
      accountId: string,
      blockNumber: number,
    }]>}
 * @param {ApiPromise} api 
 */
export async function getRules(api) {
  const c = await api.query.poe.rules.entries();
  return c.map(([key, val]) => {
    const [ruleId, payload, accountId, blockNumber] = val;
    // must hexToString to get the decoded value
    return {
      rule: payload,
      ruleId: ruleId.toString(),
      accountId: accountId.toString(),
      blockNumber: blockNumber.toNumber(),
    };
  });
}

/**
 *
 * @param api
 */
export async function getProofs(api) {
  const c = await api.query.poe.proofs.entries();
  return c.map(([key, val]) => {
    const r = {};
    val.forEach((s, k) => {
      r[k] = s;
    });
    return r;
  });
}

export function objectToString(o) {
  const r = JSON.stringify(o);
  return r;
}

/**
 * Create Rule payload and rule ID
 *
 * TODO refactor this to use unified way of creating the hashes and encoding using the network defaults
 * @param rule
 */
export async function createRulePayload(api, rule) {
  const buf = Buffer.from(objectToString(rule));
  const hash = await calculateHash(buf);
  const cid = createCID(hash);
  const payload = api.createType("Rule", rule);
  const ret = { ruleId: stringToHex(cid.toString()), payload };
  return ret;
}

/**
 * Create Rules
 * @param api
 * @param signer
 */
export async function createDefaultRules(api, signer) {
  // console.log(api.query.poe);
  // const Rule = api.createType('Rule', rule);
  // console.log(Rule);

  return Promise.all(
    [photoRule, lensRule, cameraRule].map(
      async (r) => await createRulePayload(api, r)
    )
  )
    .then((r) => {
      return saveRuleToNetwork(api, r[0], signer)
        .then(() => saveRuleToNetwork(api, r[1], signer))
        .then(() => saveRuleToNetwork(api, r[2], signer));
      // .then(() => {
      //   console.log("All rules are saved");
      //   return true;
      // })
      // .catch(console.error);
    })
    .catch(console.error);

  // const createdRule = await api.tx.poe.createRule(ruleId, rule).signAndSend(signer);
  // console.log('CreatedRule ', createdRule.toHex());
}

/**
 * Create Rule Transaction
 * @param api
 * @param param1
 * @param signer
 */
export async function saveRuleToNetwork(api, { payload, ruleId }, signer, cb) {
  // eslint-disable-next-line no-async-promise-executor
  return new Promise((resolve, reject) => {
    console.log(`\nCreating TX for the ruleId: ${ruleId}`);
    return api.tx.poe
      .createRule(ruleId, payload)
      .signAndSend(signer, {}, ({ events = [], status, isError }) => {
        console.log(`\tTransaction status:${status.type}`);

        if (status.isInBlock) {
          console.log("\tIncluded at block hash", status.asInBlock.toHex());

          console.log("\tEvents:", events.length);

          events.forEach(({ event, phase }) => {
            const { data, method, section } = event;
            const [error] = data;

            // console.log('\t', phase.toString(), `: ${section}.${method}`, data.toString());
            // console.log('\t', phase.toString(), `: ${section}.${method}`);

            if (error.isModule) {
              const {
                documentation,
                name,
                section,
              } = api.registry.findMetaError(error.asModule);
              console.error("\t", documentation.toString(), name, section);
              console.error("\tRejecting ...");

              // reject here would make all the other promises to fail
              // reject('ExtrinsicFailed');

              resolve(true);
            } else {
              console.log(
                "\t",
                phase.toString(),
                `: ${section}.${method}`,
                data.toString()
              );
              if (cb) cb([null, "Waiting for finalization"]);
            }
          });
        } else if (status.isFinalized) {
          console.log("\tFinalized block hash", status.asFinalized.toHex());
          if (cb) cb([null, "Finalized", true]);
          resolve(true);
        } else if (isError) {
          console.error(status);
          if (cb) cb([status, null]);
        }

        // console.log(
        //   `Rule created for ${ForWhat[r.forWhat]}\n hash: ${createdRuleSimple.toHex()}\n cid: ${hexToString(ruleId)}`,
        // );
      })
      .catch(reject);
  });
}

/**
 * Create Proof Transaction
 * @param api {ApiPromise}
 * @param proof {{
      "proofId": string, // hex encoded string
      "body": string, // hex encoded string
      "createdAt": string, // hex encoded string
      "parent": string, //  hex encoded string
      "ruleId": string, //  hex encoded string
      "forWhat": string, //  ForWhat enum
    }} 
 * @param signer {KeyPair} 
 * @param cb {Function} 
 */
export async function saveProofToNetwork(api, proof, signer, cb) {
  // eslint-disable-next-line no-async-promise-executor
  return new Promise((resolve, reject) => {
    console.log(
      `\nCreating TX for the proofId: ${hexToString(proof.proofId.toString())}`
    );
    if (cb) cb([null, { message: `Saving the proof to the network` }]);
    return api.tx.poe
      .createProof(proof)
      .signAndSend(signer, {}, ({ events = [], status, isError }) => {
        console.log(`\tTransaction status: ${status.type}`);
        if (cb) cb([null, { message: `Transaction status: ${status.type}` }]);

        if (status.isInBlock) {
          console.log("\tIncluded at block hash", status.asInBlock.toHex());
          if (cb)
            cb([
              null,
              {
                message: `Included at block hash: ${status.asInBlock.toHex()}`,
              },
            ]);
          console.log("\tEvents:", events.length);

          events.forEach(({ event, phase }) => {
            const { data, method, section } = event;
            const [error] = data;

            // console.log('\t', phase.toString(), `: ${section}.${method}`, data.toString());
            // console.log('\t', phase.toString(), `: ${section}.${method}`);

            if (error.isModule) {
              const {
                documentation,
                name,
                section,
              } = api.registry.findMetaError(error.asModule);
              console.error("\t", documentation.toString(), name, section);
              console.error("\tRejecting ...");
              if (cb) cb([null, { message: documentation.toString() }]);

              // reject here would make all the other promises to fail
              reject(documentation.toString());

              // resolve(true);
            } else {
              console.log(
                "\t",
                phase.toString(),
                `: ${section}.${method}`,
                data.toString()
              );
              if (cb) cb([null, { message: "Waiting for finalization" }]);
              resolve(true);
            }
          });
        } else if (status.isFinalized) {
          console.log("\tFinalized block hash", status.asFinalized.toHex());
          if (cb) cb([null, { message: "Finalized", success: true }]);
          resolve(true);
        } else if (isError) {
          console.error(status);
          if (cb) cb([status, null]);
          reject(status);
        }

        // console.log(
        //   `Rule created for ${ForWhat[r.forWhat]}\n hash: ${createdRuleSimple.toHex()}\n cid: ${hexToString(ruleId)}`,
        // );
      })
      .catch((e) => {
        console.error(e);
        reject(e);
      });
  });
}

/**
 * Find duplicates in the ops name
 * @param {*} arr
 */
export function findDuplicates(arr) {
  let sorted_arr = arr.slice().sort(); // You can define the comparing function here.
  // JS by default uses a crappy string compare.
  // (we use slice to clone the array so the
  // original array won't be modified)
  let results = [];
  for (let i = 0; i < sorted_arr.length - 1; i++) {
    if (sorted_arr[i + 1] === sorted_arr[i]) {
      results.push(sorted_arr[i]);
    }
  }
  return results;
}

/**
 * #return Promise<Buffer>
 * @param {Buffer} data
 * @param {*} algo
 * @param {*} length
 */
export async function calculateHash(data, algo = "blake2b", length = 256) {
  const hash = await mh(data, `${algo}-${length}`);
  return hash;
}
/**
 * @return {CID}
 * @param {Buffer} data
 * @param {*} codec
 */
export function createCID(data, codec = "raw") {
  return new CID(1, codec, data);
}

/**
 * Use only binary ascii representation
 * @param b {Buffer}
 * @return {Promise<string>}
 */
export async function calculatePhash(b) {
  return imghash.hash(b, 16, "binary");
}
/**
 * elapsed time, useful for performance testing
 * @param start is ret value of process.hrtime()
 * @return {number} miliseconds
 */
export const elapsedTime = (start) => {
  const elapsed = Date.now() - start;
  return elapsed;
};
