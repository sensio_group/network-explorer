import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Container,
  Dimmer,
  Divider,
  Form,
  Grid,
  Loader,
  Message,
} from "semantic-ui-react";
import { CUSTOM_TYPES } from "../../../config/common.json";
import { useSubstrate } from "../../../substrate-lib";
import {
  createRulePayload,
  findDuplicates,
  saveRuleToNetwork,
} from "../helpers";
import CreateRuleChooser from "./CreateRuleChooser";
import { cameraRule, lensRule, photoRule } from "./defaultRules";

function formatForWhat() {
  return CUSTOM_TYPES.ForWhat._enum.map((m, i) => {
    return { key: i, text: m, value: i };
  });
}
function buildCurrentOperationNames() {
  let ops = [
    ...cameraRule.ops,
    ...lensRule.ops,
    ...photoRule.ops,
    ...[cameraRule.buildParams, lensRule.buildParams, photoRule.buildParams],
  ];

  let ret = [];
  ops.forEach((o, i) => {
    if (ret.find((r) => r.value === o.op)) {
      return;
    }

    // for now don't include ops that contain other ops, they are special
    if (o.ops.length !== 0) {
      return;
    }
    ret.push({ key: i, text: o.op, value: o.op });
  });

  return ret;
}

const operations = buildCurrentOperationNames();

/**
 * Functional component for creating the Operation
 * @param {*}
 */
function Operation({ op, onHandleChange, allDisabled, defaultValue }) {
  return (
    <div>
      <Form.Group widths="equal">
        <Form.Input
          fluid
          label="Name"
          placeholder={op.name}
          defaultValue={op.name}
          onChange={onHandleChange}
          disabled={allDisabled ? true : false}
          name="name"
          input={{ maxLength: 20 }}
        />
        <Form.Input
          fluid
          label="Description"
          placeholder={op.desc}
          defaultValue={op.desc}
          onChange={onHandleChange}
          disabled={allDisabled ? true : false}
          name="desc"
        />

        <Form.Select
          fluid
          label="Operation"
          options={operations}
          defaultValue={defaultValue || operations[0].value}
          onChange={onHandleChange}
          disabled={allDisabled ? true : false}
          name="op"
        />
        <Form.Input
          fluid
          label="Hashing Algo"
          placeholder={op.hashAlgo}
          defaultValue={op.hashAlgo}
          onChange={onHandleChange}
          disabled={allDisabled ? true : false}
          name="hashAlgo"
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Input
          fluid
          label="Hashing Bits"
          type="number"
          placeholder={op.hashBits}
          defaultValue={op.hashBits}
          onChange={onHandleChange}
          disabled={allDisabled ? true : false}
          name="hashBits"
        />
        <Form.Input
          fluid
          label="Encoding Algo"
          placeholder={op.encodeAlgo}
          defaultValue={op.encodeAlgo}
          onChange={onHandleChange}
          disabled={allDisabled ? true : false}
          name="encodeAlgo"
        />
        <Form.Input
          fluid
          label="Prefix"
          placeholder={op.prefix}
          defaultValue={op.prefix}
          onChange={onHandleChange}
          disabled={allDisabled ? true : false}
          name="prefix"
        />
      </Form.Group>
    </div>
  );
}

/**
 * Create default structure for the Operation
 */
function createDefaultOperation() {
  return {
    _id: nanoid(),
    name: "Change me name",
    desc: "Longer description",
    op: operations[0].value,
    hashAlgo: "blake2b",
    hashBits: 256,
    encodeAlgo: "hex",
    prefix: "0x",
    ops: [],
  };
}

export default function CreateRule(props) {
  const defaultVal = {
    version: 1,
    name: "For flower petal",
    desc: "Generic rule for flower petal full description",
    creator: "urn:pgp:d1f5e247a976f1e3c14f0a437a6db9962ef3978e",
    forWhat: 1,
    ops: [],
    buildParams: {
      name: "Build Params",
      desc:
        "Build the payload in a way we need for this rule. Take all the values from each of the `ops",
      op: "build_params",
      hashAlgo: "",
      hashBits: 0,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
  };

  const history = useHistory();

  const { api } = useSubstrate();
  const { accountPair } = props;

  const [rule, setRule] = useState(defaultVal);
  const [errorMessage, setErrorMessage] = useState("");
  const [readyToSubmit, setReadyToSubmit] = useState(false);
  const [saving, setSaving] = useState(false);
  const [savingMessage, setSavingMessage] = useState("Saving");
  const formattedForWhat = formatForWhat();

  /**
   * MAIN SUBMIT METHOD
   */
  async function handleSubmit() {
    setErrorMessage("");
    setSaving(true);
    const opNames = rule.ops.map((o) => {
      return o.op;
    });

    const dupes = findDuplicates(opNames);

    if (dupes.length !== 0) {
      setErrorMessage(
        `Duplicates in ops, check the Operation field in Operations. Dupes: [${dupes.join(
          ","
        )}]`
      );
      setSaving(false);
    }
    const ruleOpsClean = rule.ops.map(({ _id, ...rest }) => {
      return rest;
    });
    const rulePayload = await createRulePayload(api, {
      ...rule,
      ops: ruleOpsClean,
    });

    await saveRuleToNetwork(
      api,
      rulePayload,
      accountPair,
      ([error, message, success]) => {
        if (error) {
          console.error(error);
          alert(error);
          setSaving(false);
        } else {
          setSavingMessage(message);
          if (success) {
            setSavingMessage(message);
            setTimeout(() => {
              setSaving(false);
              history.push("/poe/rules");
            }, 700);
          }
        }
      }
    );
  }

  function handleValueChange(e, { name, value }) {
    e.preventDefault();

    setRule({
      ...rule,
      [name]: value,
    });
  }

  function handleOperationValueChange(e, { name, value }, opId) {
    e.preventDefault();
    const op = rule.ops.find((o) => o._id === opId);
    const opIndex = rule.ops.findIndex((o) => o._id === opId);
    const ops = rule.ops;
    op[name] = value;

    ops[opIndex] = op;
    console.log(ops);
    setRule({
      ...rule,
      ops,
    });
  }
  function handleChangeBuildParams(e, { name, value }) {
    e.preventDefault();

    setRule({
      ...rule,
      buildParams: { [name]: value },
    });
  }

  function addNewOperation() {
    const ops = rule.ops;
    ops.unshift(createDefaultOperation());
    setRule({
      ...rule,
      ops,
    });
  }

  function deleteOperation(id) {
    const ops = rule.ops.filter((f) => f._id !== id);
    setRule({
      ...rule,
      ops,
    });
  }

  useEffect(() => {
    if (rule.ops.length >= 1) {
      setReadyToSubmit(true);
    }
  }, [rule]);
  const MessageExampleFloating = () => (
    <Message negative floating>
      {errorMessage}
    </Message>
  );

  return (
    <Container>
      <Dimmer active={saving} inverted>
        <Loader>{savingMessage} ...</Loader>
      </Dimmer>

      <Grid>
        <Grid.Row color={"purple"}>
          <Grid.Column width={4}>
            <h1 style={{ marginLeft: 16 }}>Create PoE Rule</h1>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <CreateRuleChooser accountPair={accountPair} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          {errorMessage && <MessageExampleFloating />}
          <Grid.Column>
            <Form>
              <Form.Input
                fluid
                label="Version"
                placeholder={rule.version}
                defaultValue={rule.version}
                type="number"
                onChange={handleValueChange}
                name="version"
              />
              <Form.Input
                fluid
                label="Name"
                placeholder={rule.name}
                defaultValue={rule.name}
                onChange={handleValueChange}
                name="name"
                input={{ maxLength: 20 }}
              />
              <Form.Input
                fluid
                label="Description"
                placeholder={rule.desc}
                defaultValue={rule.desc}
                onChange={handleValueChange}
                name="desc"
              />
              <Form.Input
                fluid
                label="Creator"
                placeholder={rule.creator}
                defaultValue={rule.creator}
                onChange={handleValueChange}
                name="creator"
              />
              <Form.Select
                fluid
                label="For What"
                options={formattedForWhat}
                defaultValue={rule.forWhat}
                onChange={handleValueChange}
                name="forWhat"
              />
              <Divider />
              <div>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <h3>Operations: {rule.ops.length}</h3>
                  <Form.Button color="purple" onClick={addNewOperation}>
                    Add new operation
                  </Form.Button>
                </div>
                <Divider />
                <div>
                  {rule.ops.map((op, i) => (
                    <div key={op._id} style={{ marginBottom: 16 }}>
                      <div style={{ marginBottom: 16 }}>
                        <h3>#{i + 1}</h3>
                      </div>
                      <div>
                        <Operation
                          op={op}
                          onHandleChange={(e, t) =>
                            handleOperationValueChange(e, t, op._id)
                          }
                        />
                        {rule.ops.length !== 1 && (
                          <Form.Button
                            disabled={rule.ops.length === 1}
                            onClick={() => deleteOperation(op._id)}
                            color="red"
                          >
                            Delete operation :: {op._id}
                          </Form.Button>
                        )}
                      </div>
                    </div>
                  ))}
                </div>
              </div>

              <Divider />

              <div>
                <div style={{ marginBottom: 16 }}>
                  <div style={{ marginBottom: 16 }}>
                    <h3>Build Params</h3>
                  </div>
                  <div>
                    <Operation
                      op={rule.buildParams}
                      defaultValue={
                        operations.find((o) => o.value === rule.buildParams.op)
                          .value
                      }
                      onHandleChange={handleChangeBuildParams}
                      allDisabled
                    />
                  </div>
                </div>
              </div>

              <Form.Button
                onClick={handleSubmit}
                disabled={!readyToSubmit}
                primary
              >
                Submit
              </Form.Button>
            </Form>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
