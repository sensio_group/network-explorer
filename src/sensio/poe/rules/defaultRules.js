export const defaultCreator =
  "urn:pgp:d1f5e247a976f1e3c14f0a437a6db9962ef3978e";

export const lensRule = {
  version: 1,
  name: "Sensio Lens rule",
  desc: "Operations for Lens verification",
  creator: defaultCreator,
  forWhat: 3,
  ops: [
    {
      desc: "Extract LensSerialNumber from Metadata",
      op: "meta_lens_serial_number", // this is the name of the param
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc: "Extract LensInfo from Metadata",
      op: "meta_lens_info",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc: "Extract LensModel from Metadata",
      op: "meta_lens_model",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
  ],
  buildParams: {
    desc:
      "Build the payload in a way we need for this rule. Take all the values from each of the `ops",
    op: "build_params",
    hashAlgo: "",
    hashBits: 0,
    encodeAlgo: "hex",
    prefix: "0x",
    ops: [],
  },
};

export const cameraRule = {
  version: 1,
  name: "Sensio Camera rule",
  desc: "Operations for Camera verification",
  creator: defaultCreator,
  forWhat: 2,
  ops: [
    {
      desc: "Extract SerialNumber from Metadata",
      op: "meta_serial_number", // this is the name of the param
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc: "Extract Make from Metadata",
      op: "meta_make",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc: "Extract Model from Metadata",
      op: "meta_model",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
  ],
  buildParams: {
    desc:
      "Build the payload in a way we need for this rule. Take all the values from each of the `ops",
    op: "build_params",
    hashAlgo: "",
    hashBits: 0,
    encodeAlgo: "hex",
    prefix: "0x",
    ops: [],
  },
};

export const photoRule = {
  version: 1,
  name: "Sensio Photo rule",
  desc: "Rule for creating hashes and PoE for any Photo",
  creator: defaultCreator,
  forWhat: 1,
  ops: [
    {
      desc:
        "Hash of full unchanged metadata buffer (or similar). Without raw pixels",
      op: "metadata_hash",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc:
        "Metadata must be removed and has must be created off of the RAW PIXELS",
      op: "raw_pixels_hash",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc:
        "Perceptual hash calculation, currently implementing http://blockhash.io/",
      op: "perceptual_hash",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc:
        "Document ID. The common identifier for all versions and renditions of a resource. Found under xmp.did:GUID and parsed only the GUID part without the namespace xmp.did:",
      op: "meta_document_id",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc:
        "Original Document ID. The common identifier for the original resource from which the current resource is derived. For example, if you save a resource to a different format, then save that one to another format, each save operation should generate a new xmpMM:DocumentID that uniquely identifies the resource in that format, but should retain the ID of the source file here.",
      op: "meta_original_document_id",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc: "XMP date time original field",
      op: "meta_date_time_original",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc: "XMP create date",
      op: "meta_create_date",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc: "XMP copyright field",
      op: "meta_copyright",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: [],
    },
    {
      desc: "Import all the rules from the camera rule",
      op: "poe_camera_id",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: cameraRule.ops,
    },
    {
      desc: "Import all the rules from the lens rule",
      op: "poe_lens_id",
      hashAlgo: "blake2b",
      hashBits: 256,
      encodeAlgo: "hex",
      prefix: "0x",
      ops: lensRule.ops,
    },
  ],
  buildParams: {
    desc:
      "Build the payload in a way we need for this rule. Take all the values from each of the `ops",
    op: "build_params",
    hashAlgo: "",
    hashBits: 0,
    encodeAlgo: "hex",
    prefix: "0x",
    ops: [],
  },
};
