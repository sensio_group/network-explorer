import React from "react";
import { Grid, List } from "semantic-ui-react";

export default function PoEModule(props) {
  return (
    <Grid>
      <Grid.Row color={"purple"}>
        <h1 style={{ marginLeft: 16 }}>Proof Of Existence Pallet</h1>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          The ultimate goal of this pallet is to create verifiable proof for any
          kind of data. If you can turn data into the <code>Buffer</code> you
          can probably create the PoE. Idea is simple, provide set of rules,
          apply them on the data, take the output and create the proof. This
          sound like any hashing function, in essence it is with the difference
          that in applying just the hashing function on the data we end up with
          the single value without any descriptors about the data, in some cases
          we don't even know which hash is used if not specified or implemented
          as multihash, but applying the rule which contains the list of
          operations where each operation produces <code>single</code> hash then
          combining that into structured way for humans, then creating content
          address we enable interoperability between any producer of the proof.
          Theoretically the proof can be generated using the any kind of
          language and stored anywhere, it can be verified if 3 conditions are
          met: The <code>Proof</code> generation depends on the{" "}
          <code>Rule</code> definition and its operations.
          <List as="ol">
            <List.Item as="li">
              Rule must be valid and verifiable and retrieved from the
              SensioNetwork
            </List.Item>
            <List.Item as="li">Rule must be applied exactly as it is</List.Item>
            <List.Item as="li">
              Proof ID must be broadcasted and available
            </List.Item>
            <List.Item as="li">
              The data can be private and never leave the users domain of
              control (Optional, it can be public, encrypted ....)
            </List.Item>
          </List>
          Follow the numbers in the navigation, they will help you get the PoE.
          Also you might want to switch the Account from Alice to Bob then
          create new proofs and see what will happen.
          <br></br>
          <br></br>
          To learn more about Sensio why not check out our{" "}
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://wiki.sensio.dev"
          >
            wiki.
          </a>
          <br></br>
          Our{" "}
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://discordapp.com/invite/WHe4EuY"
          >
            discord server is the best place to get in contact with us and ask
            questions.
          </a>
          <br></br>
          <br></br>
          Have fun and stay safe 🖖
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
