import { hexToString, stringToU8a } from "@polkadot/util";
import ExifReader from "exifreader";
import { imageSize } from "image-size";
import {
  calculateHash,
  calculatePhash,
  createCID,
  elapsedTime,
  objectToString,
} from "../helpers";

/**
 * Return the sizes of an image
 * @return {{height: number, width: number, type: string}}
 * @param {FileReader} fileReader
 */
export function getImageSizes(fileReader) {
  const arrayBuff = base64ImageTo(fileReader.result, "Buffer");
  const sizeOf = imageSize(arrayBuff);
  return sizeOf;
}

/**
 * Transform base64 image path to array buffer
 * @return {Buffer | ArrayBuffer}
 * @param {*} base64
 */
export function base64ImageTo(base64, toWhat) {
  toWhat = toWhat || "ArrayBuffer";
  let res;

  // eslint-disable-next-line no-useless-escape
  base64 = base64.replace(/^data\:([^\;]+)\;base64,/gim, "");
  switch (toWhat) {
    case "Binary":
      res = atob(base64);
      break;
    case "ArrayBuffer":
      const binary = base64ImageTo(base64, "Binary");
      res = new ArrayBuffer(binary.len);
      break;
    case "Buffer":
      res = Buffer.from(base64, "base64");
      break;

    default:
      throw new Error("Unsupported transformation " + toWhat);
  }

  return res;
}

/**
 * At the end we must get the calculated hashes
 * content_hash - hex encoded
 * metadata_hash - hex encoded
 * perceptual_hash - hex encoded ???
 *
 * @param {Rule} rule
 * @param {Buffer} file
 * @param {Buffer} fileWithoutMetadata
 * @return {Promise<string[]>}
 */

export async function executeOps(operations, file, rawPixels) {
  const tags = ExifReader.load(file);
  return Promise.all(
    operations.map(async (operation) => {
      const { op: OperationName, hashAlgo, hashBits, ops } = operation;
      // eslint-disable-next-line prefer-const
      let ret = "",
        b,
        h,
        mh;
      const op = hexToString(OperationName.toString());
      const hashAlgoDecoded = hexToString(hashAlgo.toString());
      const hashBitsDecoded = hashBits.toNumber();

      switch (op) {
        case "perceptual_hash":
          const phash = await calculatePhash(file);
          if (ops.length) {
            throw new Error(
              `ExecuteOps:: OP has more ops, we don't support that just yet.`
            );
          }
          ret = phash;
          break;
        case "meta_image_description":
          ret = h;
          break;
        case "metadata_hash":
          b = stringToU8a(objectToString(tags));
          mh = await calculateHash(b, hashAlgoDecoded, hashBitsDecoded);
          h = createCID(mh);
          if (ops.length) {
            throw new Error(
              `ExecuteOps:: OP has more ops, we don't support that just yet.`
            );
          }
          ret = h.toString();
          break;
        case "raw_pixels_hash":
          // here we should take the raw pixel hash only
          mh = await calculateHash(file, hashAlgoDecoded, hashBitsDecoded);
          h = createCID(mh);

          if (ops.length) {
            throw new Error(
              `ExecuteOps:: OP has more ops, we don't support that just yet.`
            );
          }
          ret = h.toString();
          break;

        case "meta_document_id":
          const { DocumentID } = tags;
          if (DocumentID && DocumentID.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(DocumentID.value?.split(":")[1]),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;

        case "meta_original_document_id":
          const { OriginalDocumentID } = tags;
          if (OriginalDocumentID && OriginalDocumentID.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(formatToUUID(OriginalDocumentID.value)),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_create_date":
          const { CreateDate } = tags;
          if (CreateDate && CreateDate.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(CreateDate.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_date_time_original":
          const { DateTimeOriginal } = tags;
          if (DateTimeOriginal && DateTimeOriginal.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(DateTimeOriginal.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_copyright":
          const { Copyright } = tags;
          if (Copyright && Copyright.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(Copyright.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_serial_number":
          const { SerialNumber } = tags;
          if (SerialNumber && SerialNumber.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(SerialNumber.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_make":
          const { Make } = tags;
          if (Make && Make.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(Make.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_model":
          const { Model } = tags;
          if (Model && Model.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(Model.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_lens_serial_number":
          const { LensSerialNumber } = tags;
          if (LensSerialNumber && LensSerialNumber.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(LensSerialNumber.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_lens_info":
          const { LensInfo } = tags;
          if (LensInfo && LensInfo.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(LensInfo.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;
        case "meta_lens_model":
          const { LensModel } = tags;
          if (LensModel && LensModel.value) {
            h = createCID(
              await calculateHash(
                Buffer.from(LensModel.value.toString()),
                hashAlgoDecoded,
                hashBitsDecoded
              )
            );
            if (ops.length) {
              throw new Error(
                `ExecuteOps:: OP has more ops, we don't support that just yet.`
              );
            }
            ret = h.toString();
          }
          break;

        case "poe_camera_id":
          let ecOps = await executeOps(ops, file, rawPixels);

          h = createCID(
            await calculateHash(
              Buffer.from(objectToString(buildBody(ops, ecOps))),
              hashAlgoDecoded,
              hashBitsDecoded
            )
          );
          ret = h.toString();
          break;
        case "poe_lens_id":
          let elOps = await executeOps(ops, file, rawPixels);

          h = createCID(
            await calculateHash(
              Buffer.from(objectToString(buildBody(ops, elOps))),
              hashAlgoDecoded,
              hashBitsDecoded
            )
          );
          ret = h.toString();
          break;

        default:
          throw new Error("No default operation accepted." + op);
      }
      return ret;
    })
  );
}

/**
 * Map the executed ops to the ops itself. could be good thing for refactor in the future
 * @param rule {Rule}
 * @param executedOps  {string[]}
 * @return { { [ k:string ] : any } }
 */

function buildBody(ops, executedOps) {
  // eslint-disable-next-line prefer-const
  let body = {};
  ops.forEach((op, i) => {
    const opName = hexToString(op.op.toString());
    return (body[opName] = executedOps[i]);
  });
  return body;
}

/**
 *
 * @param {Rule} rule
 * @param {string} ruleId
 * @param {any} body
 * @param {string} creator
 * @param {*} prev
 * @return {Promise<{
      "proofId": string, // hex encoded string
      "body": string, // hex encoded string
      "createdAt": string, // hex encoded string
      "prev": string, //  hex encoded string
      "ruleId": string, //  hex encoded string
      "forWhat": string, //  ForWhat enum
    }>}
 */
async function buildProof(api, rule, ruleId, params = {}, owner, prev = "") {
  const ruleIdDecoded = hexToString(ruleId);
  const body = {
    params,
    owner: createCID(await calculateHash(owner)).toString(),
    ruleId: ruleIdDecoded,
    forWhat: rule.forWhat,
  };

  const bodyString = objectToString(body);
  const proofId = createCID(
    await calculateHash(Buffer.from(bodyString))
  ).toString();

  const proof = {
    proofId,
    body: bodyString,
    createdAt: Date.now(),
    prev,
    ruleId: ruleIdDecoded,
    forWhat: rule.forWhat.toString(),
  };
  return api.createType("Proof", proof);
}

/**
 * Original Document ID sometimes is a UUID without hyphens, this makes it look nice
 * later maybe add the is-uuid check https://runkit.com/woss/is-uuid-guid
 * @param str {string}
 * @return  {string}
 */
export function formatToUUID(uuid) {
  // return the parse one, no check here

  if (uuid.split("-").length === 5) {
    return uuid;
  }

  const r = new RegExp(
    /([A-Za-z0-9]{8})([A-Za-z0-9]{4})([A-Za-z0-9]{4})([A-Za-z0-9]{4})([A-Za-z0-9]{12})/gi
  );

  const matches = r.exec(uuid);
  if (matches.length !== 6) {
    throw new Error("Invalid UUID");
  }

  const ret = matches.slice(1, matches.length).join("-").toLowerCase();

  return ret;
}

/**
 * Execute the rule, its operations and building the payload
 * @param {{ rule: Rule, rawPixels: Buffer, file: Buffer, creator: string, ruleId: string }} params
 * @param {function} cb
 * @return {Promise<{payload: any, proof:string}>}
 */
export async function executeRule(api, params, cb) {
  try {
    const mainPerf = Date.now();
    const { rule, rawPixels, file, ruleId, creator } = params;

    let perf = Date.now();
    const ops = await executeOps(rule.ops, file, rawPixels);
    cb([null, { message: "Executing OPS", elapsedTime: elapsedTime(perf) }]);

    perf = Date.now();
    const body = buildBody(rule.ops, ops);
    cb([null, { message: "Building Body", elapsedTime: elapsedTime(perf) }]);

    perf = Date.now();
    const payload = await buildProof(api, rule, ruleId, body, creator);
    cb([null, { message: "Building Proof", elapsedTime: elapsedTime(perf) }]);

    cb([
      null,
      {
        message: "Executed",
        success: true,
        elapsedTime: elapsedTime(mainPerf),
      },
    ]);

    return payload;
  } catch (error) {
    console.error(error);
    cb([error]);
    return null;
  }
}
