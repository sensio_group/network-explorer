# Proof-of-Existence Pallet

The ultimate goal of this pallet is to create verifiable proof for any kind of data. If you can turn data into the `Buffer` you can probably create the PoE.

Idea is simple, provide set of rules, apply them on the data, take the output and create the proof.

This sound like any hashing function, in essence it is with the difference that in applying just the hashing function on the data we end up with the single value without any descriptors about the data, in some cases we don't even know which hash is used if not specified or implemented as multihash, but applying the rule which contains the list of operations where each operation produces `single` hash then combining that into structured way for humans, then creating content address we enable interoperability between any producer of the proof. Theoretically the proof can be generated using the any kind of language and stored anywhere, it can be verified if 3 conditions are met:

1. Rule must be valid and verifiable and retrieved from the SensioNetwork
2. Rule must be applied exactly as it is
3. Proof ID must be broadcasted and available
4. The data can be private and never leave the users domain of control (Optional, it can be public, encrypted ....)

The `Proof` generation depends on the `Rule` definition and its operations.

Follow the numbers in the navigation, they will help you get the PoE.

To learn more about Sensio why not check out our [wiki](https://wiki.sensio.dev) and join our [discord server](https://discordapp.com/invite/WHe4EuY)
