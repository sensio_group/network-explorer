import { hexToString } from "@polkadot/util";
import { nanoid } from "nanoid";
import React, { createRef, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Container,
  Dimmer,
  Feed,
  Form,
  Grid,
  Image as ImageComponent,
  Input,
  Loader,
  Message,
  Segment,
} from "semantic-ui-react";
import { useSubstrate } from "../../../substrate-lib";
import { getRules, saveProofToNetwork } from "../helpers";
import { base64ImageTo, executeRule, getImageSizes } from "./execution";

export default function CreateProof({ accountPair }) {
  const { api } = useSubstrate();
  const history = useHistory();
  const [saving, setSaving] = useState(false);
  const [disableSaveToNetwork, setDisableSaveToNetwork] = useState(true);
  const [savingMessage, setSavingMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState();
  const [proofSaved, setProofSaved] = useState(false);
  const [feed, setFeed] = useState([]);

  const [rules, setRules] = useState([]);
  const [selectableRules, setSelectableRules] = useState([]);

  const [file, setFile] = useState();
  const [fileReader, setFileReader] = useState();
  const [imagePreview, setImagePreview] = useState("");

  const defaultPayloadHolder = {
    rule: null,
    ruleId: null,
    creator: "",
  };
  const [proofPayload, setProofPayload] = useState(defaultPayloadHolder);
  const [proof, setProof] = useState();

  let reader;
  // Ref for out canvas where we remove the metadata
  let canvasRef = createRef();

  function onFileLoaded() {
    setImagePreview(reader.result);
    setFileReader(reader);
  }

  // Callback function for when a new file is selected.
  const handleFileChosen = (incomingFile) => {
    setDisableSaveToNetwork(true);
    if (!incomingFile) return null;

    reader = new FileReader();
    reader.onload = () => {
      Object.assign(incomingFile, {
        preview: URL.createObjectURL(incomingFile),
      });
    };
    reader.onloadend = onFileLoaded;
    reader.readAsDataURL(incomingFile);
    setFile(incomingFile);
  };

  async function getRawPixels() {
    return new Promise((resolve, reject) => {
      let canvas = canvasRef.current;
      let ctx = canvas.getContext("2d");
      const { width, height } = getImageSizes(fileReader);

      const newImage = new Image();
      newImage.src = fileReader.result;

      canvas.width = width;
      canvas.height = height;

      // newImage.onload = () => {};
      ctx.drawImage(newImage, 0, 0, width, height);
      ctx.canvas.toBlob(
        async (blob) => {
          // const f = new File([blob], file.name, {
          //   type: `image/${type}`,
          // });

          // console.log("New file size diff from original: ", file.size - f.size);
          return blob.arrayBuffer().then((r) => resolve(Buffer.from(r)));
        },
        "image/jpeg",
        1
      );
    });
  }

  function handleChangeTheRule(e, { value }) {
    setDisableSaveToNetwork(true);
    setProofPayload({
      ...proofPayload,
      rule: rules[value].rule,
      ruleId: rules[value].ruleId,
    });
  }
  function onHandleChange(e, { name, value }) {
    setDisableSaveToNetwork(true);

    setProofPayload({
      ...proofPayload,
      [name]: value,
    });
  }

  /**
   *
   * @param {{ message: string, success: boolean, elapsedTime: number}} obj
   */
  function addNewFeed(obj) {
    const { message, elapsedTime } = obj;
    const newFeeds = feed;
    newFeeds.unshift({ message, elapsedTime });
    setFeed(newFeeds);
  }

  /**
   * PROCESS RULE  METHOD
   */
  async function handleProcessRule() {
    setProofSaved(false);
    setSaving(true);
    setErrorMessage("");
    setDisableSaveToNetwork(true);
    setSavingMessage("Implementing the rule");
    const rawPixels = await getRawPixels();
    const payload = await executeRule(
      api,
      {
        ...proofPayload,
        file: base64ImageTo(fileReader.result, "Buffer"),
        rawPixels,
      },
      ([error, res]) => {
        if (error) {
          console.error(error);
          setErrorMessage(error);
          return;
        }
        const { message, success } = res;

        addNewFeed(res);
        setSavingMessage(message);
        if (success) {
          setSaving(false);
          setDisableSaveToNetwork(false);
        }
      }
    ).catch((error) => {
      console.error(error);
      setErrorMessage(error);
    });
    setProof(payload);
    setSavingMessage("");
    setSaving(false);
    setDisableSaveToNetwork(false);
  }

  /**
   * SAVE TO NETWORK
   */
  async function handleSaveToNetwork() {
    setProofSaved(false);
    setErrorMessage("");
    setSaving(true);
    setDisableSaveToNetwork(true);
    setSavingMessage("Saving to the Network, check console for more info");
    try {
      await saveProofToNetwork(api, proof, accountPair, ([error, res]) => {
        if (error) {
          console.error(error);
          setErrorMessage(error);
          return;
        }
        const { success } = res;

        addNewFeed(res);

        if (success) {
          setProofSaved(true);
          setSaving(false);
          setDisableSaveToNetwork(false);
        }
      });
    } catch (error) {
      console.error(error);
      setErrorMessage(error.toString());
      setSavingMessage(error);
      setSaving(false);
      setDisableSaveToNetwork(false);
    }
  }

  useEffect(() => {
    const getInfo = async () => {
      try {
        const r = await getRules(api);
        if (r.length === 0) {
          history.push("/poe/rules");
        } else {
          setRules(r);

          const _rules = r
            // eslint-disable-next-line array-callback-return
            .map(({ rule, ruleId }, i) => {
              if (rule.forWhat.toString() === "Photo") {
                const name = hexToString(rule.name.toString());
                const v = rule.version.toNumber();

                return {
                  key: i,
                  text: `v${v} - ${name}`,
                  value: i,
                };
              }
            })
            .filter((e) => e); // remove undefined and empty

          setSelectableRules(_rules);
        }
      } catch (e) {
        console.error(e);
      }
    };
    getInfo();
  }, [api, history, file]);

  return (
    <Container>
      <Grid>
        <Grid.Row color={"purple"}>
          <Grid.Column width={4}>
            <h1 style={{ marginLeft: 16 }}>PoE Proofs</h1>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column width="6">
            <Grid.Row>
              <Form success={false} warning={false}>
                <Form.Field>
                  {/* File selector with a callback to `handleFileChosen`. */}
                  <Input
                    type="file"
                    id="image"
                    accept="image/jpeg"
                    label="Select image"
                    onChange={(e) => handleFileChosen(e.target.files[0])}
                  />
                  {/* Show this message if the file is available to be claimed */}
                </Form.Field>
                <Form.Input
                  fluid
                  label="Creator"
                  placeholder="urn | DID | SSI | email"
                  onChange={onHandleChange}
                  name="creator"
                />
                <Form.Select
                  fluid
                  label="Select Rule"
                  options={selectableRules}
                  onChange={handleChangeTheRule}
                  name="rule"
                />
                <Form.Group widths="equal">
                  <Form.Button
                    disabled={
                      proofPayload.rule &&
                      fileReader &&
                      proofPayload.creator &&
                      !saving
                        ? false
                        : true
                    }
                    primary
                    onClick={handleProcessRule}
                  >
                    1. Implement the Rule
                  </Form.Button>
                  <Form.Button
                    disabled={disableSaveToNetwork}
                    color="green"
                    onClick={handleSaveToNetwork}
                  >
                    2. Create the Proof
                  </Form.Button>
                </Form.Group>
              </Form>
            </Grid.Row>

            <Grid.Row style={{ marginTop: 16 }}>
              <Grid.Column>
                <h3
                  style={{
                    backgroundColor: "#a333c8",
                    color: "white",
                    padding: 16,
                  }}
                >
                  Feed
                </h3>
              </Grid.Column>
              <Grid.Column>
                <Feed style={{ overflow: "auto", maxHeight: 250 }}>
                  {feed.map((f) => {
                    return (
                      <Feed.Event style={{ marginTop: 16 }} key={nanoid()}>
                        <Feed.Content>
                          {f.elapsedTime && (
                            <Feed.Date>Took: {f.elapsedTime} ms</Feed.Date>
                          )}
                          <Feed.Summary>{f.message}</Feed.Summary>
                        </Feed.Content>
                      </Feed.Event>
                    );
                  })}
                </Feed>
              </Grid.Column>
            </Grid.Row>
          </Grid.Column>
          <Grid.Column width="10">
            <Segment placeholder style={{ alignItems: "center" }}>
              <Dimmer active={saving} inverted>
                <Loader>{savingMessage.toString()} ...</Loader>
              </Dimmer>

              <ImageComponent src={imagePreview} />
              <canvas
                style={{ display: "none" }}
                ref={canvasRef}
                width={640}
                height={425}
              />
            </Segment>
            {errorMessage && (
              <Message
                error
                header="There was some errors with your submission"
                content={errorMessage}
              />
            )}

            {proofSaved && (
              <Message
                success
                header="Proof is created!"
                content={`Proof ID is ${hexToString(
                  proof.proofId.toString()
                )} You better copy it 😉`}
              />
            )}
          </Grid.Column>
        </Grid.Row>
        {proof && (
          <Grid.Row>
            <Grid.Row style={{ width: "100%" }}>
              <Grid.Column>
                <h3
                  style={{
                    backgroundColor: "#a333c8",
                    color: "white",
                    padding: 16,
                  }}
                >
                  DEBUG
                </h3>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <pre style={{ overflow: "scroll" }}>
                  {JSON.stringify(
                    {
                      proof,
                      bodyDecoded: JSON.parse(
                        hexToString(proof.body.toString())
                      ),
                    },
                    null,
                    2
                  )}
                </pre>
              </Grid.Column>
            </Grid.Row>
          </Grid.Row>
        )}
      </Grid>
    </Container>
  );
}
