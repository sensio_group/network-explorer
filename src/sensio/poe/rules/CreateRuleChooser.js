import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Divider,
  Grid,
  Header,
  Icon,
  Loader,
  Segment,
} from "semantic-ui-react";
import { useSubstrate } from "../../../substrate-lib";
import { createDefaultRules } from "../helpers";

export default function CreateRuleChooser(props) {
  const { accountPair } = props;
  const { api } = useSubstrate();
  const [creating, setCreating] = useState(false);
  const [savingMessage, setSavingMessage] = useState(
    "Saving, check console for more info"
  );

  function handleClickCreateDefaultRules(e) {
    e.preventDefault();
    setCreating(true);
    createDefaultRules(api, accountPair).then((r) => {
      if (r) {
        setSavingMessage("");
        setCreating(false);
      }
    });
  }
  return (
    <Segment placeholder>
      <Loader active={creating} inline="centered">
        {savingMessage} ...
      </Loader>
      <Grid columns={2} stackable textAlign="center">
        <Divider vertical>Or</Divider>

        <Grid.Row verticalAlign="middle">
          <Grid.Column>
            <Header icon>
              <Icon name="file outline" />
            </Header>
            <Button color="orange" disabled={creating}>
              <Link style={{ color: "white" }} to="/poe/create-rule">
                Create Custom Rule
              </Link>
            </Button>
          </Grid.Column>

          <Grid.Column>
            <Header icon>
              <Icon name="copy outline" />
            </Header>
            <Button
              disabled={creating}
              onClick={handleClickCreateDefaultRules}
              primary
            >
              Create Default Rules
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
}
