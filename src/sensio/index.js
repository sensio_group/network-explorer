import React from "react";
import { Container } from "semantic-ui-react";
import PoEModule from "./poe";
export default function SensioModule(props) {
  const { accountPair } = props;

  return (
    <Container>
      <PoEModule accountPair={accountPair} />
    </Container>
  );
}
