import { hexToString } from "@polkadot/util";
import React, { useEffect, useState } from "react";
import { Container, Grid, Icon, List } from "semantic-ui-react";
import { useSubstrate } from "../../../substrate-lib";
import colors from "../../colors.json";
import CodeModal from "../CodeModal";
import { getRules } from "../helpers";
import CreateRuleChooser from "./CreateRuleChooser";
export default function RulesList({ accountPair }) {
  const { api } = useSubstrate();
  const [rules, setRules] = useState([]);
  const [interval, setCurrentInterval] = useState(0);

  useEffect(() => {
    async function getInfo() {
      try {
        const r = await getRules(api);
        setRules(r);
      } catch (e) {
        console.error(e);
      }
    }
    if (!interval) {
      getInfo();
    } else {
      setCurrentInterval(setInterval(getInfo, 3000));
    }
    return () => {
      clearInterval(interval);
    };
  }, [api, interval]);

  return (
    <Container>
      <Grid>
        <Grid.Row color={"purple"}>
          <Grid.Column width={4}>
            <h1 style={{ marginLeft: 16 }}>PoE Rules</h1>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column>
            <CreateRuleChooser accountPair={accountPair} />
          </Grid.Column>
        </Grid.Row>

        {rules.length !== 0 && (
          <Grid.Row>
            <Grid.Column>
              <List divided verticalAlign="middle" size="large">
                {rules.map(
                  ({ rule, ruleId, accountNumber, blockNumber }, i) => {
                    const { forWhat, version, desc, name } = rule;
                    const color = colors[i];

                    return (
                      <List.Item key={hexToString(ruleId.toString())}>
                        <List.Content floated="right">
                          <CodeModal data={rule} />
                        </List.Content>
                        <Icon
                          style={{
                            color: color,
                            fontSize: "2em",
                            paddingTop: 10,
                          }}
                          name="circle thin"
                        />

                        <List.Content>
                          <List.Header>
                            {hexToString(name.toString())}
                          </List.Header>
                          <List.Header></List.Header>
                          <List.Description>
                            This is the rule for {forWhat.toString()} and
                            version v{version.toString()}
                            <List.Description>
                              {hexToString(desc.toString())}
                            </List.Description>
                          </List.Description>
                        </List.Content>
                      </List.Item>
                    );
                  }
                )}
              </List>
            </Grid.Column>
          </Grid.Row>
        )}
      </Grid>
    </Container>
  );
}
